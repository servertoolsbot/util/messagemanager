package com.gitlab.servertoolsbot.util.messagemanager;

import com.gitlab.servertoolsbot.util.messagemanager.util.GetObject;
import com.google.gson.JsonObject;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

class MessageCache {
    List<Message> fromCode = new ArrayList<>();
    List<Message> fromStorage = new ArrayList<>();
    HashMap<HashMap<String, Locale>, Message> messages = new HashMap<>();

    MessageCache() {

    }

    private Message getPlainMessage(String id, Locale locale) {
        return messages.get(buildIdHashMap(id, locale));
    }

    String getMessage(String id, Locale locale) {
        return getPlainMessage(id, locale).getValue();
    }

    String getParsedMessage(String id, Locale locale, MessageReplacement... replacedVariables) {
        String toReturn = this.getMessage(id, locale);
        Message msg = this.getPlainMessage(id, locale);
        for (String key : msg.getVariables().keySet()) {
            String replacement = msg.getVariables().get(key);
            for (MessageReplacement replacedVariable : replacedVariables) {
                if (replacedVariable.getVariableId().equalsIgnoreCase(key)) {
                    replacement = replacedVariable.getVariableReplacement();
                }
            }
            toReturn = toReturn.replace(
                    MessageManager.getInstance().getVariableFormat().replace("%$1",
                            key)
                    , replacement);
        }
        return toReturn;
    }

    void load() {
        instance = this;
        this.messages.clear();
        this.fromStorage.clear();

        this.loadFromStorage();

        List<Message> overwrittenMsgs = new ArrayList<>();

        this.fromStorage.forEach(messageStorage -> this.fromCode.forEach(messageCode -> {
            if (messageStorage.getId().equalsIgnoreCase(messageCode.getId()) && messageStorage.getLocale().equals(messageCode.getLocale()))
                overwrittenMsgs.add(messageStorage);
        }));

        if (overwrittenMsgs.isEmpty()) overwrittenMsgs.add(new Message());

        HashMap<HashMap<String, Locale>, Boolean> overwriteChecker = new HashMap<>();

        this.fromCode.forEach(messageCode -> overwrittenMsgs.forEach(overwritten -> {
            if (messageCode.getId().equalsIgnoreCase(overwritten.getId()) && messageCode.getLocale().equals(overwritten.getLocale())) {
                fromStorage.forEach(messageStorage -> {
                    if (messageStorage.getId().equalsIgnoreCase(overwritten.getId()) && messageStorage.getLocale().equals(overwritten.getLocale())) {
                        messageCode.getVariables().keySet().forEach(key -> messageStorage.addVariable(key, messageCode.getVariables().get(key)));
                        this.messages.put(buildIdHashMap(messageStorage.getId(), messageStorage.getLocale()), messageStorage);
                        overwriteChecker.put(buildIdHashMap(messageStorage.getId(), messageStorage.getLocale()), true);
                    }
                });
            } else {
                if (!overwriteChecker.getOrDefault(buildIdHashMap(messageCode.getId(), messageCode.getLocale()), false))
                    this.messages.put(buildIdHashMap(messageCode.getId(), messageCode.getLocale()), messageCode);
            }
        }));
    }

    private void loadFromStorage() {
        MessageStorageType type = MessageManager.getInstance().getStorageType();
        if (type == MessageStorageType.JSON && MessageManager.getInstance().getStorageType().getMessageFolder() != null)
            Storage.Json.load();
        if (type == MessageStorageType.MARIADB)
            Storage.MariaDB.load();
    }


    void addMessage(Message msg) {
        this.fromCode.add(msg);
    }

    void addMessages(Message... msgs) {
        for (Message msg : msgs) this.addMessage(msg);
    }

    void removeMessage(Message msg) {
        fromCode.forEach(message -> {
            if (message.getId().equalsIgnoreCase(msg.getId()) && message.getLocale() == msg.getLocale()) {
                fromCode.remove(message);
                addMessage(msg);
            }
        });
    }

    void removeMessages(Message... msgs) {
        for (Message msg : msgs) removeMessage(msg);
    }

    private HashMap<String, Locale> buildIdHashMap(String id, Locale locale) {
        HashMap<String, Locale> idMap = new HashMap<String, Locale>();
        idMap.put(id, locale);
        return idMap;
    }

    private static class Storage {
        static class Json {
            static void load() {
                List<File> languageFiles = getLanguageFiles();
                if (languageFiles.isEmpty()) return;
                languageFiles.forEach(file -> {
                    Locale locale = new Locale(file.getName().replace(".json", ""));
                    JsonObject json = GetObject.get(file);
                    json.keySet().forEach(key -> instance.fromStorage.add(new Message(key, json.get(key).getAsString()).inLocale(locale)));
                });
            }

            static List<File> getLanguageFiles() {
                List<File> languageFiles = new ArrayList<>();
                File[] listOfFiles = MessageManager.getInstance().getStorageType().getMessageFolder().listFiles();

                if (listOfFiles == null) return new ArrayList<>();

                for (int i = 0; i < listOfFiles.length; i++) {
                    if (listOfFiles[i].isFile()) {
                        languageFiles.add(listOfFiles[i]);
                    }
                }

                return languageFiles;
            }
        }

        static class MariaDB {
            static void load() {
                MessageStorageType type = MessageManager.getInstance().getStorageType();
                String table = type.getTable();
                try {
                    Connection connection = DriverManager.getConnection(
                            "jdbc:mariadb://" + type.getHost() + ":3306/"
                            + type.getDatabase()
                            + "?user=" + type.getUser()
                            + "&password=" + type.getPassword()
                    );
                    ResultSet resultSet = connection.createStatement().executeQuery("SELECT * FROM " + table + ";");
                    while (resultSet.next()) {
                        instance.fromStorage.add(
                                new Message(resultSet.getString("id"),
                                        resultSet.getString("value"))
                                        .inLocale(new Locale(resultSet.getString("locale")))
                        );
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static MessageCache instance = null;

    private static MessageCache getInstance() {
        return instance;
    }
}
