package com.gitlab.servertoolsbot.util.messagemanager;

/**
 * Replaces a variable in a {@link Message}
 */
public class MessageReplacement {
    private String variableId = "";
    private String variableReplacement = "";

    /**
     * Constructs a new MessageReplacement instance. {@link MessageReplacement#MessageReplacement(String, String)} is preferred over this.
     */
    public MessageReplacement() {

    }


    /**
     * @param variableId          variable id. Uses {@link MessageReplacement#setVariableId(String)}
     * @param variableReplacement variable replacement. Uses {@link MessageReplacement#setVariableReplacement(String)}
     */
    public MessageReplacement(String variableId, String variableReplacement) {
        this.setVariableId(variableId);
        this.setVariableReplacement(variableReplacement);
    }

    /**
     * @param variableId new variable id
     * @return           MessageReplacement object with new variable id
     */
    public MessageReplacement setVariableId(String variableId) {
        this.variableId = variableId;
        return this;
    }

    /**
     * @return variable id of the MessageReplacement
     */
    public String getVariableId() {
        return this.variableId;
    }

    /**
     * @param variableReplacement new variable replacement
     * @return                    MessageReplacement object with the new variable replacement
     */
    public MessageReplacement setVariableReplacement(String variableReplacement) {
        this.variableReplacement = variableReplacement;
        return this;
    }

    /**
     * @return variable replacement of the MessageReplacement
     */
    public String getVariableReplacement() {
        return this.variableReplacement;
    }
}
