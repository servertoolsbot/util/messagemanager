package com.gitlab.servertoolsbot.util.messagemanager;

import java.io.File;

/**
 * Possible MessageStorageTypes
 */
public enum MessageStorageType {

    /**
     * Uses JSON files as the storage
     */
    JSON,

    /**
     * Uses a MariaDB database as the storage
     */
    MARIADB,

    /**
     * Default, needs to be changed using {@link MessageManager#setStorageType(MessageStorageType)}
     */
    UNDEFINED;


    /**
     * JSON related settings
     */
    private File messageFolder = new File("languages");

    /**
     * MariaDB related settings
     */
    private String user;
    private String password;
    private String database;
    private String table;
    private String host;

    /**
     * Setup of Json
     * @param messageFolder the folder used to get data as JSON files
     */
    public void setupJson(File messageFolder) {
        this.setMessageFolder(messageFolder);
    }
    public void setMessageFolder(File messageFolder) {
        this.messageFolder = messageFolder;
    }
    public File getMessageFolder() {
        return this.messageFolder;
    }

    /**
     * Setup of MariaDB
     * @param user      the user to the access the database
     * @param password  the password of the user
     * @param database  the database name
     * @param table     the table name
     * @param host      the hostname or ip address
     */
    public void setupMariaDB(String user, String password, String database, String table, String host) {
        this.setUser(user);
        this.setPassword(password);
        this.setDatabase(database);
        this.setTable(table);
        this.setHost(host);
    }
    public void setUser(String user) {
        this.user = user;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public void setDatabase(String database) {
        this.database = database;
    }
    public void setTable(String table) {
        this.table = table;
    }
    public void setHost(String host) {
        this.host = host;
    }
    public String getUser() {
        return this.user;
    }
    public String getPassword() {
        return this.password;
    }
    public String getDatabase() {
        return this.database;
    }
    public String getTable() {
        return this.table;
    }
    public String getHost() {
        return this.host;
    }

}
