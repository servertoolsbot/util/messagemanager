package com.gitlab.servertoolsbot.util.messagemanager.util;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class GetObject {
    public static JsonObject get(File file) {
        try (FileReader reader = new FileReader(file)) {
            return new JsonParser().parse(reader).getAsJsonObject();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return new JsonObject();
    }
}
