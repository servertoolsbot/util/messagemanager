package com.gitlab.servertoolsbot.util.messagemanager;

import java.util.Locale;

/**
 * Main entry point for adding, removing and querying messages.
 */
public class MessageManager {
    private MessageStorageType storageType = MessageStorageType.UNDEFINED;

    private String defaultVariableValue = "";

    private Locale defaultLocale = new Locale("en");
    private MessageCache cache = new MessageCache();
    boolean cacheLoaded = false;
    private static MessageManager instance = null;

    private String variableFormat = "%{%$1}";

    /**
     * Constructs a new MessageManager instance
     */
    public MessageManager() {
        this.instance = this;
    }

    /**
     * @param type the MessageStorageType to use
     * @return     MessageManager object with the new MessageStorageType
     */
    public MessageManager setStorageType(MessageStorageType type) {
        this.storageType = type;
        return this;
    }

    /**
     * @return the MessageStorageType of this instance
     */
    public MessageStorageType getStorageType() {
        return this.storageType;
    }

    /**
     * Can't be used currently
     *
     * @param cache the MessageCache to use
     * @return      MessageManager object with new MessageCache
     */
    public MessageManager setMessageCache(MessageCache cache) {
        this.cache = cache;
        return this;
    }

    /**
     * @param locale the default {@link Locale} to use
     * @return       MessageManager object with new default locale
     */
    public MessageManager setDefaultLocale(Locale locale) {
        this.defaultLocale = locale;
        return this;
    }

    /**
     * @param defaultVariableValue the default variable value to use
     * @return                     MessageManager object with new default variable value
     */
    public MessageManager setDefaultVariableValue(String defaultVariableValue) {
        this.defaultVariableValue = defaultVariableValue;
        return this;
    }

    /**
     * @return default variable value for this messager instance
     */
    public String getDefaultVariableValue() {
        return this.defaultVariableValue;
    }

    /**
     * @return default {@link Locale} being used
     */
    public Locale getDefaultLocale() {
        return this.defaultLocale;
    }

    /**
     * @param newFormat new format that will be used
     * @return          MessageMnager object with the new format
     */
    public MessageManager setVariableFormat(String newFormat) {
        this.variableFormat = newFormat;
        return this;
    }

    /**
     * @return the variable format
     */
    public String getVariableFormat() {
        return this.variableFormat;
    }

    /**
     * @param msg the {@link Message} to add
     * @return    the MessageManager with the message added
     */
    public MessageManager addMessage(Message msg) {
        this.cache.addMessage(msg);
        return this;
    }

    /**
     * @param msgs the {@link Message}s to add
     * @return     the MessageManager with the messages added
     */
    public MessageManager addMessages(Message... msgs) {
        this.cache.addMessages(msgs);
        return this;
    }

    /**
     * @param msg the {@link Message} to remove
     * @return    the MessageManager with the message removed
     */
    public MessageManager removeMessage(Message msg) {
        this.cache.removeMessage(msg);
        return this;
    }

    /**
     * @param msgs the {@link Message}s to remove
     * @return     the MessageManager with the messages removed
     */
    public MessageManager removeMessages(Message... msgs) {
        this.cache.removeMessages(msgs);
        return this;
    }

    /**
     * @param id the id of the {@link Message} to get
     * @return   the {@link Message} content as a String
     */
    public String getMessage(String id) {
        if (!cacheLoaded)
            throw new IllegalStateException("Can't request message data, that isn't used, please use MessageManager#load");
        if (this.getStorageType() == MessageStorageType.JSON && this.getStorageType().getMessageFolder() == null)
            throw new IllegalStateException("Can't load strings from not specified storage files");
        return this.getMessageInLocale(id, getDefaultLocale());
    }

    /**
     * @param id     the id of the {@link Message} to get
     * @param locale the {@link Locale} to get the message in
     * @return       the {@link Message} content as a String
     */
    public String getMessageInLocale(String id, Locale locale) {
        return this.cache.getMessage(id, locale);
    }

    /**
     * @param id     the id of the {@link Message} to get
     * @param locale the {@link Locale} as a String to get the message in
     * @return       the {@link Message} content as a String
     */
    public String getMessageInLocale(String id, String locale) {
        return this.getMessageInLocale(id, new Locale(locale));
    }

    /**
     * @param id           the id of the {@link Message} to parse
     * @param locale       the {@link Locale} to parse the message in
     * @param replacements the possible variable replacements to take
     * @return             the {@link Message} content parsed with variables as a String
     */
    public String getParsedMessageInLocale(String id, Locale locale, MessageReplacement... replacements) {
        if (!cacheLoaded)
            throw new IllegalStateException("Can't request message data, that isn't used, please use MessageManager#load");
        if (this.getStorageType() == MessageStorageType.JSON && this.getStorageType().getMessageFolder() == null)
            throw new IllegalStateException("Can't load strings from not specified storage files");
        return this.cache.getParsedMessage(id, locale, replacements);
    }

    /**
     * @param id           the id of the {@link Message} to parse
     * @param replacements the possible variable replacements to take
     * @return             the {@link Message} content parsed with variables as a String
     */
    public String getParsedMessage(String id, MessageReplacement... replacements) {
        return getParsedMessageInLocale(id, this.getDefaultLocale(), replacements);
    }

    /**
     * Loads messages from the storage. Needs to be called atleast once before any messages can be queried.
     */
    public void load() {
        this.cacheLoaded = true;
        this.cache.load();
    }

    static MessageManager getInstance() {
        return instance;
    }
}
