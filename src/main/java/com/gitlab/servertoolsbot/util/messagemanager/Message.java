package com.gitlab.servertoolsbot.util.messagemanager;

import java.util.HashMap;
import java.util.Locale;

/**
 * Contains all information a message
 */
public class Message {
    private String id = "";
    private String value = "";
    private HashMap<String, String> variables = new HashMap<>();
    private Locale locale = MessageManager.getInstance().getDefaultLocale();


    /**
     * Constructs a new Message object
     *
     * @param id    id of the new Message object
     * @param value value of the Message object
     */
    public Message(String id, String value) {
        this.setId(id);
        this.setValue(value);
    }
    Message() {

    }

    /**
     * @param id id to set
     * @return   Message object with new id
     */
    public Message setId(String id) {
        this.id = id;
        return this;
    }

    /**
     * @param value value to set
     * @return      Message object with new value
     */
    public Message setValue(String value) {
        this.value = value;
        return this;
    }

    /**
     * @param locale {@link Locale} to set
     * @return       Message object with new Locale
     */
    public Message inLocale(Locale locale) {
        this.locale = locale;
        return this;
    }

    /**
     * @return the {@link Locale} of the Message object
     */
    public Locale getLocale() {
        return this.locale;
    }

    /**
     * @return the id of the Message object
     */
    public String getId() {
        return this.id;
    }

    /**
     * @return the value of the Message object
     */
    public String getValue() {
        return this.value;
    }

    /**
     * {@link #addVariable(String, String)} is preferred over this
     *
     * @param id the id of the variable
     * @return   Message object with the variable added
     */
    public Message addVariable(String id) {
        this.addVariable(id, MessageManager.getInstance().getDefaultVariableValue());
        return this;
    }

    /**
     * Preferred over {@link #addVariable(String)}
     *
     * @param id           the id of the variable
     * @param defaultValue the default value of the variable
     * @return             Message object with the variable added
     */
    public Message addVariable(String id, String defaultValue) {
        this.variables.put(id, defaultValue);
        return this;
    }

    /**
     * @return the variables as a {@link HashMap}
     */
    public HashMap<String, String> getVariables() {
        return this.variables;
    }
}
