package com.gitlab.servertoolsbot.test;

import com.gitlab.servertoolsbot.util.messagemanager.Message;
import com.gitlab.servertoolsbot.util.messagemanager.MessageManager;
import com.gitlab.servertoolsbot.util.messagemanager.MessageReplacement;
import com.gitlab.servertoolsbot.util.messagemanager.MessageStorageType;

import java.util.Locale;

public class Main {
    public static void main(String[] args) {
        MessageManager mm = new MessageManager();

        MessageStorageType type = MessageStorageType.MARIADB;
        type.setupMariaDB(
                "wikidevbot",
                "wikidevbotpassword",
                "wikidevbot",
                "messages",
                "127.0.0.1"
        );

        mm.setStorageType(type);
        mm.setDefaultLocale(Locale.ENGLISH);


        mm.addMessage(new Message("test.msg", "Very interesting %{test}").addVariable("test", "Toby"));
        mm.addMessage(new Message("test.msg", "Sehr interressant %{test}").addVariable("test", "Tobi").inLocale(Locale.GERMAN));

        mm.load();

        System.out.println(mm.getParsedMessage("test.msg", new MessageReplacement().setVariableId("test").setVariableReplacement("Markus")));
    }
}
